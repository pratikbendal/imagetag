import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';

import { AppComponent } from './app.component';
import {MatTableModule} from '@angular/material/table';
import { NgxPaginationModule } from 'ngx-pagination';
import { Router } from '@angular/router'; 
import { RouterModule, Routes } from '@angular/router';
import { ImagetagFormComponent } from './imagetag-form/imagetag-form.component';
import { AppRoutingModule } from './app.routing';
import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import {MatRadioModule} from '@angular/material/radio';
import { ImagetabrowComponent } from './imagetabrow/imagetabrow.component';

const routes: Routes=[
  {
    path:'',
    component:ImagetagFormComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    ImagetagFormComponent,
    ImagetabrowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatTableModule,
    NgxPaginationModule,
    MatRadioModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
