import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagetagFormComponent } from './imagetag-form.component';

describe('ImagetagFormComponent', () => {
  let component: ImagetagFormComponent;
  let fixture: ComponentFixture<ImagetagFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagetagFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagetagFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
