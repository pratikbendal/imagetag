import { Component, OnInit} from '@angular/core';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes} from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import { Router } from '@angular/router'; 
import { NgxPaginationModule } from 'ngx-pagination';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route } from '@angular/router';
import {MatRadioModule} from '@angular/material/radio';
import {MatTableModule} from '@angular/material/table';
import { ConstantPool } from '@angular/compiler/src/constant_pool';
@Component({
  selector: 'app-imagetag-form',
  templateUrl: './imagetag-form.component.html',
  styleUrls: ['./imagetag-form.component.css']
})
export class ImagetagFormComponent implements OnInit {
token:string;

Back=["Backless", "Covered Back","Deep Back","Sheer Back", "Cut-Out Deta","Other"];
NeckLine=[ "Boat", "Square", "V", "Round","Halter neck", "Sweetheart", "Collar","Other"];
Shoulder=["Cape", "Off-shoulder", "Normal", "Cold-shoulder", "One-shoulder","Other"];
Sleeves=["Short/Cap/Half", "Three-quarter", "Full Sleeves", "Sleeveless","Other"];

ngBack=[];
backError:boolean;

ngNeck=[];
neckError:boolean;

ngSeleevs=[];
sleevesError:boolean;

ngShoulder=[];
shoulderError:boolean;

nginput=[];
spinner:boolean;
untaggefimageFound:boolean;
noimagefoundTagging:boolean;
inputBox:boolean;
data:any;
parentId:any;
type=[];
collection=[];
itemperPage:any;
actualCount:any;
dataSource=[];
pagenoActive: number=1;
 
discription=[];

  constructor(private http:Http,private route: ActivatedRoute,private router:Router)
   {
    this.spinner=true;
   this.http.get('http://node-staging.flyrobeapp.com:7999/v2/get_parent_id_list/?page=1').subscribe((response)=>
   {
    
    this.data=response.json().data
    this.itemperPage=response.json().page_count;
    this.actualCount=response.json().total_count;
     this.spinner=false;
    if(this.actualCount!=0)
    {
      this.untaggefimageFound=true;
    }
    else
    {
      this.noimagefoundTagging=true;
    }
   
     })
  

   }
   
  ngOnInit() {
  }

  getServerData(page:number,row_no)
  {
    window.scrollTo(0,0);
    this.spinner=true;
    this.pagenoActive = page ;
    this.http.get('http://node-staging.flyrobeapp.com:7999/v2/get_parent_id_list/?page='+page).subscribe((response)=>
    {
          this.ngBack[row_no]=null;
          this.ngNeck[row_no]=null;
          this.ngSeleevs[row_no]=null;
          this.ngShoulder[row_no]=null;
          this.data=response.json().data
   })
 
   
   this.spinner=false;
  }


  putApi(row_no)
  { 
    
 
    this.parentId=this.data[row_no].parent_id
  
    if(this.ngBack[row_no]==null)
    {
       alert("Select Back")
    }
  else if(this.ngNeck[row_no]==null)
    {
      alert("Select Neck")
    }
  else if(this.ngSeleevs[row_no]==null)
    {
      alert("Select Seleeves")
    }
  else if(this.ngShoulder[row_no]==null)
    {
      alert("Select Shoulder")
    }
    else{
  //  console.log(this.ngBack[row_no])
  //  console.log(this.ngNeck[row_no])
  //  console.log(this.ngShoulder[row_no])
  //  console.log(this.ngSeleevs[row_no])
   
 
   const data={
      "back_type":this.ngBack[row_no],
      "neckline_type":this.ngNeck[row_no],
      "shoulder_type":this.ngShoulder[row_no],
      "sleeves_type":this.ngSeleevs[row_no]
    }
    this.http.put('http://node-staging.flyrobeapp.com:7999/tag_image/'+this.parentId,data).subscribe(
      (putresponse)=>{
        if(putresponse.json().status==200)
        {
          this.ngBack[row_no]=null;
          this.ngNeck[row_no]=null;
          this.ngSeleevs[row_no]=null;
          this.ngShoulder[row_no]=null;
        }  
        else
        {
         alert("Something Went to Wrong") 
        }
      }
    )
    if(this.data!=-1)
    {
      this.data.splice(row_no,1)
    }
    
   }
  }
 
  skipButton(row_no)
  {
    if(this.data!=-1)
    {
      this.data.splice(row_no,1);
      this.ngBack[row_no]=null;
          this.ngNeck[row_no]=null;
          this.ngSeleevs[row_no]=null;
          this.ngShoulder[row_no]=null;
    }
  }
}
