import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ImagetagFormComponent } from './imagetag-form/imagetag-form.component'
import { HttpClient } from '@angular/common/http';
const routes: Routes = [
    { path: '', component: ImagetagFormComponent },
   
    ];
@NgModule({
    imports: [
    RouterModule.forRoot(routes)
    ],
    exports: [
    RouterModule
    ],
    declarations: []
    })
    export class AppRoutingModule { }