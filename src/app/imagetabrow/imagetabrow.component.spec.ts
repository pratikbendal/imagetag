import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagetabrowComponent } from './imagetabrow.component';

describe('ImagetabrowComponent', () => {
  let component: ImagetabrowComponent;
  let fixture: ComponentFixture<ImagetabrowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagetabrowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagetabrowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
